
$length = 0
function translate {
    if ($args[0] -match 'a'){
        return 'Alpha'
    }
    if ($args[0] -match 'b'){
        return 'Bravo'
    }
    if ($args[0] -match 'c'){
        return 'Charlie'
    }
    if ($args[0] -match 'd'){
        return 'Delta'
    }
    if ($args[0] -match 'e'){
        return 'Echo'
    }
    if ($args[0] -match 'f'){
        return 'Foxtrot'
    }
    if ($args[0] -match 'g'){
        return 'Golf'
    }
    if ($args[0] -match 'h'){
        return 'Hotel'
    }
    if ($args[0] -match 'i'){
        return 'India'
    }
    if ($args[0] -match 'j'){
        return 'Juliet'
    }
    if ($args[0] -match 'k'){
        return 'Kilo'
    }
    if ($args[0] -match 'l'){
        return 'Lima'
    }
    if ($args[0] -match 'm'){
        return 'Mike'
    }
    if ($args[0] -match 'n'){
        return 'November'
    }
    if ($args[0] -match 'o'){
        return 'Oscar'
    }
    if ($args[0] -match 'p'){
        return 'Papa'
    }
    if ($args[0] -match 'q'){
        return 'Quebec'
    }
    if ($args[0] -match 'r'){
        return 'Romeo'
    }
    if ($args[0] -match 's'){
        return 'Sierra'
    }
    if ($args[0] -match 't'){
        return 'Tango'
    }
    if ($args[0] -match 'u'){
        return 'Uniform'
    }
    if ($args[0] -match 'v'){
        return 'Victor'
    }
    if ($args[0] -match 'w'){
        return 'Whiskey'
    }
    if ($args[0] -match 'x'){
        return 'X-Ray'
    }
    if ($args[0] -match 'y'){
        return 'Yankee'
    }
    if ($args[0] -eq '1'){
        return 'One'
    }
    if ($args[0] -eq '2'){
        return 'Two'
    }
    if ($args[0] -eq '3'){
        return 'Three'
    }
    if ($args[0] -eq '4'){
        return 'Four'
    }
    if ($args[0] -eq '5'){
        return 'Five'
    }
    if ($args[0] -eq '6'){
        return 'Six'
    }
    if ($args[0] -eq '7'){
        return 'Seven'
    }
    if ($args[0] -eq '8'){
        return 'Eight'
    }
    if ($args[0] -eq '9'){
        return 'Nine'
    }
    if ($args[0] -match '0'){
        return 'Zero'
    }
    else {
        Write-Host $args[0] -NoNewline
    }
}

function write-nato{
    $string = $args[0]
    $l = 0
    while ($l -lt $string.Length){
        $string[$l..($l+3)] | ForEach-Object {Write-Host $_ -NoNewline}
        $l = $l+3
        if ($l -lt $string.Length){
            Write-Host "-" -NoNewline
        }
    }
    Write-Host "`n `n"
    while ( $length -lt $string.Length ) {
        $t = translate $string[$length]
        if ((($length+1) %4) -eq 0 ){
            Write-Host "$t " 
        }    
        else {
            write-host "$t " -NoNewline
        }
        $length ++
        if ($length % $args[1] -eq 0){
            Write-Host "`n==================================="
        }
        
    }
}
Export-ModuleMember -Function write-nato